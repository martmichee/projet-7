import React from 'react';
import { useState } from 'react';
import { UploadPicture } from '../../actions/user_actions';
import { useDispatch, useSelector } from "react-redux";



const Uploadimg = () => {
    const [file, setFile] = useState();
    const dispatch =  useDispatch();
    const userData = useSelector((state) => state.userReducer)

    const handlePicture = (e) => {
        e.preventDefault();
        const data = new FormData();
        data.append("name", userData.lastName);
        data.append('userId', userData._id);
        data.append('file', file);

        dispatch(UploadPicture(data, userData._id));
    }
    return (
        <form action="" onSubmit={handlePicture} className="upload-pic">
            <label htmlFor="file"> changer l'image</label>
            <input type="file" id='file' name='file' accept='.jpg, .jpeg, .png'
            onChange={(e) => setFile(e.target.files[0])}/>
        <br />
        <input type="submit" value="Envoyer"/>
        </form>
    );
};

export default Uploadimg;