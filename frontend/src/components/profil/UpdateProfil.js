import React, { useState } from 'react';
import LeftNav from '../LeftNav';
import { useDispatch, useSelector } from 'react-redux';
import Uploadimg from './Uploadimg';
import { updateBio } from '../../actions/user_actions';




const UpdateProfil = () => {
    const [bio, setBio] = useState('');
    const [updateForm, setUpdateForm] = useState(false);
    const userData  = useSelector((state) => state.userReducer)
    const dispatch = useDispatch();

    const handleUpdate = () => {
dispatch(updateBio (userData._id, bio));
setUpdateForm(false);
    }
    
    
    
    return (
        <div className="profil-page">
            <LeftNav/>
            <h1> Profil de {userData.lastName}</h1>
            <div className="update-container">
                <div className="left-part">
                    <h3> Photo de profil</h3>
                    {/* <img src={userData.picture} alt="user-pic" /> */}
                    <img src="../asset/img/clochette.png" alt="" />
                   <Uploadimg/>
              
                </div>
                <div className="right-part">
                    <div className="bio-update">
                        <h3>Bio</h3>
                        {updateForm === false && (
                            <>
                            {/* <p onClick= {() => setUpdateForm(!updateForm)}>{userData.bio} </p> */}
                            <p onClick= {() => setUpdateForm(!updateForm)}>Je suis la fée clochette qui rit, qui pète </p> 
                            <button className='update-button' onClick={() => setUpdateForm(!updateForm)}> Modifier la Bio</button>
                            {updateForm && (
                                <>
                                
                                <textarea  type='text' defaultValue={userData.bio} onChange={(e) => setBio(e.target.value)}></textarea>
                            <button onClick={handleUpdate}> Valider les modifications </button>
                            </>
                            )}
                            </>
                        )}
                    </div>
                    <h5> Membre depuis le  {userData.createdAt}</h5>
                </div>
            </div>
        </div>
    );
};

export default UpdateProfil;