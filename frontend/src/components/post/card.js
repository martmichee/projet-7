import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { isEmpty } from "../Utils";
import LikeButton from "./LikeButton";
import { updatePost } from "../../actions/post_actions";
import DeleteCard from "./DeleteCard";
import CardComments from "./CardComments";


const Card = ({post}) => {
    const [isLoading, setIsLoading]  = useState(true);
    const [isUpdated, setIsUpdated] = useState(false);
        const [textUpdate, setTextUpdate] = useState(null);
        const [showComments, setShowComments] = useState(false);
        const usersData = useSelector((state) => state.usersReducer);
    const userData = useSelector((state) => state.userReducer);
    const dispatch = useDispatch();
    
    const updateItem = () => {
    if (textUpdate) {
        dispatch(updatePost(post._id, textUpdate));
      }
      setIsUpdated(false);
    };
    useEffect(() => {
        !isEmpty(usersData[0]) && setIsLoading(false);
    }, [usersData])
   
    return (
        <li className='card-container' key={post._id}>
            {isLoading ? (
                <i className='fas fa-spinner fa-spin'></i>
            ) : (
              <li className='card-container'>
              <>
              <div className="card-left">
                  <img src={
                      !isEmpty(usersData[0]) &&
                        usersData.map((user) => {
                            if (user._id === post.posterId) return user.picture
                            else  return null
                        }).join('')
                        
                  } alt="poster-pic" />
                        
                         <img src="../asset/img/clochette.png" alt="" />
              </div>
              <div className="card-right">
                  <div className="card-header">
                      <div className="pseudo">
                          <h3>
                              {!isEmpty(userData[0]) &&
                              userData
                              .map((user) => {
                                  if (user._id === post.posterId) return user.lastName
                                  else  return null
                              })}
                            
                          </h3>
                      </div>
                      <span>{post.createdAt}</span>
                 
                  </div>
                  {isUpdated === false && <p>{post.message}</p>}
            {isUpdated && (
              <div className="update-post">
                <textarea
                  defaultValue={post.message}
                  onChange={(e) => setTextUpdate(e.target.value)}
                />
                <div className="button-container">
                  <button className="btn" >
                  onClick={updateItem}
                    
                  </button>
                </div>
              </div>
            )}
                  <p>coucou les copains</p>
                  {post.picture && (<img src={post.picture} alt="card-pic" className='card-pic' />)}
               {post.video && (
              <iframe
                width="500"
                height="300"
                src={post.video}
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
                title={post._id}
              ></iframe>
            )} 
            {userData._id === post.posterId && (
              <div className="button-container">
                <div onClick={() => setIsUpdated(!isUpdated)}>
                  <img src="../asset/img/edit.svg" alt="edit" />
                </div>
                <DeleteCard id={post._id} />
              </div>
            )}
            <img className='card-pic'  src="../asset/img/nature.jpg" alt="white heart" />
            <div className="card-footer">
                <div className="comment-icon">
                    <img src="../asset/img/message1.svg" alt="comment" />
                   
                   <span>3</span> 
                </div>
                <img 
                  onClick={() => setShowComments(!showComments)}
                src="../asset/img/heart-filled.svg" alt="red heart" />
                
                <LikeButton post={post} />
                <LikeButton/>
            </div>
            {showComments && <CardComments post={post} />}
              </div>
              </>  
              
     </li>
            )
        }
    
         </li>
       
    );
};

export default Card