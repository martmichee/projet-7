const router = require ('express').Router();
const auth = require('../middleware/auth_middleware');
const authController = require('../controllers/comments_controller')

router.post('/', auth, authController.createComment);
router.get('/', auth, authController.getAllComments); 
router.get('/:userId', auth, authController.getOneComment);
router.delete('/:id', auth, authController.deleteComment );


module.exports = router;
