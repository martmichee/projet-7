const router = require('express').Router();
const auth = require ('../middleware/auth_middleware');
const authController = require('../controllers/user_controller');
// const auth_controller = require('../controllers/auth_controller')



// router.post('/login',  authController.login, );
// router.post("/register",  authController.register, );
// router.get("/register", authController.register);

router.post("/register",  authController.register);
router.post("/login",  authController.login, authController.signIn);


router.get('/', authController.getAllAccount);
router.get('/:_id', authController.getOneAccount);
router.put('/:_id', auth, authController.modifyAccount);
router.delete('/:_id', auth, authController.deleteAccount);

module.exports = router;