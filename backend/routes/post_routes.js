const router = require ('express').Router();
const auth = require('../middleware/auth_middleware');
const authController = require('../controllers/posts_controller');

router.post('/', auth, authController.createPost); 
router.get('/:userid', auth, authController.getOnePost);
router.get('/', auth, authController.getAllPosts);
router.put('/:id', auth, authController.modifyPost);
router.delete('/:id', auth, authController.deletePost );


module.exports = router;