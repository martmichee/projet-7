const sequelize = require('../connectBd')
const { DataTypes, Model } = require('sequelize');
const User = sequelize.define("user", {
  firstName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  lastName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false
  }, 
  picture: {
    type: String,
    default: "../public/asset/img/profil_compte.png"
  },
  bio :{
    type: String,
    max: 1024,
  },
  
  // likes: {
  //   type: String
  // },
  });


module.exports = User