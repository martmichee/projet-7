const sequelize = require('../connectBd')
const {
    DataTypes,
    Model
} = require('sequelize');


const Comment = sequelize.define("comment", {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
    },

    Text: {
        type: DataTypes.STRING,
        allowNull: false
    },
});
Comment.associate = function (models) {
    Comment.belongsTo(models.User, {
        foreignKey: 'userId',

    });
    Comment.belongsTo(models.Message, {
        foreignKey: 'commentId',
        onDelete: 'CASCADE',
    });

    return Comment;

}
module.exports = Comment