const express = require("express");
const userRoutes = require('./routes/user_routes');
const postsRoutes = require('./routes/post_routes');
const commentsRoutes = require('./routes/comment_routes')
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const {checkUser, requireAuth} = require('./controllers/auth_controller');
const cors = require('cors');
const auth = require ('./middleware/auth_middleware');



const app = express();
require('dotenv').config({
  path: './config/.env'
});
const helmet = require('helmet');

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});
const corsOptions = {
  origin: process.env.CLIENT_URL,
  credentials: true,
  'allowedHeaders': ['sessionId', 'Content-Type'],
  'exposedHeaders': ['sessionId'],
  'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
  'preflightContinue': false
}

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));


// jwt
// app.get('*', checkUser);
// app.get('/jwtid', requireAuth, (req, res) => {
//   res.status(200).send(res.locals.user._id)
// });
app.get("/", auth, (req, res) => {
  res.status(200).send("Welcome 🙌 ");
});

// routes
app.use('/api/user', userRoutes);
app.use('/api/posts', postsRoutes);
app.use('/api/comments', commentsRoutes);
app.use(helmet());



// server
app.listen(process.env.PORT, () => {
  console.log(`listening on ${process.env.PORT}`);
});