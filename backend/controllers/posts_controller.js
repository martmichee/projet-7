const Post = require('../models/posts_model');
const canUpdateOrDelete = require ('./authorization')


module.exports = {
    // créer un post
    createPost: async function (req, res, next) {
        //   delete req.body._id;
        const post = await Post.create({
            iduser: req.body.id,
            title: req.body.title,
            text: req.body.text,
            image: "",
        });
        res.status(200).json({
            post
        });
        console.log(post);
        post.save()
            .then(() => res.status(201).json({
                message: 'post enregistré !'
            }))
            .catch(error => res.status(400).json({
                error
            }));
    },


    //   / modifier un post
    modifyPost: async function (req, res, next) {
        Post.findOne({
                where: {
                    id: req.params.id
                }
            })
            .then((post) => {
                canUpdateOrDelete(post.userId)
                title = req.body.title;
                comment = req.body.comment;
                Post.update()
                    .then(() => res.status(201).json({
                        message: 'Post modifié !'
                    }))
                    .catch(error => res.status(400).json({
                        error: error.message
                    }));
            })
            .catch(error => res.status(500).json({
                error: error.message
            }));
    },

    // voir un post
    getOnePost: async function (req, res, next) {
        Post.findOne({
                _id: req.params.id
            })
            .then(post => (res.status(200).json(post)))
            .catch(error => res.status(404).json({
                error: error.message
            }));
    },

    // voir tous les post
    getAllPosts: async function (req, res, next) {
        Post.find()
            .then(posts => (res.status(200).json(posts)))
            .catch(error => res.status(400).json({
                error: error.message
            }));
    },

    // supprimer un post
    deletePost: async function (req, res, next) {
        Post.findOne({
                _id: req.params.id
            })
            .then(post => {
                canUpdateOrDelete(post.userId)
                const filename = post.imageUrl.split('/images/')[1];
                fs.unlink(`images/${filename}`, () => {
                    Post.deleteOne({
                            _id: req.params.id
                        })
                        .then(() => res.status(200).json({
                            message: 'post supprimé !'
                        }))
                        .catch(error => res.status(400).json({
                            error: error.message
                        }));
                });
            })
            .catch(error => res.status(500).json({
                error
            }));
    }
}