const Comment = require('../models/posts_model')
module.exports = {

    // créer un commentaire
    createComment: async function (req, res, next) {
        //   delete req.body._id;
        const comment = await Comment.create({
            // userId: req.decodedToken.userId,
            postId: req.body.postId,
            text: req.body.text,
            image: "",
        });
        res.status(200).json({
            comment
        });
        console.log(comment);
        comment.save()
            .then(() => res.status(201).json({
                message: 'commentaire envoyé !'
            }))
            .catch(error => res.status(400).json({
                error
            }));
    },


    //   / modifier un commentaire
    modifyComment: async function (req, res, next) {
        Comment.findOne({
                where: {
                    id: req.params.id
                }
            })
            .then((comment) => {  
                canUpdateOrDelete(comment.userId)              
                commentId = req.body.commentId;
                comment.update()
                    .then(() => res.status(201).json({
                        message: 'Commentaire modifié !'
                    }))
                    .catch(error => res.status(400).json({
                        error: error.message
                    }));
            })
            .catch(error => res.status(500).json({
                error: error.message
            }));
    },

    // voir un commentaire
    getOneComment: async function (req, res, next) {
        Comment.findOne({
            where : {postId: req.params.id}
            })
            .then(comment => (res.status(200).json(comment)))
            .catch(error => res.status(404).json({
                error: error.message
            }));
    },

    // voir tous les commentaires
    getAllComments: async function (req, res, next) {
        Comment.find(
            {where : {postId: req.params.id}}
        )
            .then(comments => (res.status(200).json(comments)))
            .catch(error => res.status(400).json({
                error: error.message
            }));
    },

    // supprimer un commentaire
    deleteComment: async function (req, res, next) {
        Comment.findOne({
                _id: req.params.id
            })
            .then(comment => {
                canUpdateOrDelete(post.userId);
                const filename = comment.imageUrl.split('/images/')[1];
                fs.unlink(`images/${filename}`, () => {
                    Comment.destroy({
                            _id: req.params.id
                        })
                        .then(() => res.status(200).json({
                            message: 'commentaire supprimé !'
                        }))
                        .catch(error => res.status(400).json({
                            error: error.message
                        }));
                });
            })
            .catch(error => res.status(500).json({
                error
            }));
    }
}