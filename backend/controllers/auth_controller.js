const UserModel = require('../models/user_model');
const jwt = require('jsonwebtoken');
const { signUpErrors, signInErrors } = require('../utils/errors');

const maxAge = 3 * 24 * 60 * 60 * 1000;

const createToken = (id) => {
  return jwt.sign({id}, process.env.RANDOM_TOKEN_SECRET, {
    expiresIn: maxAge
  })
};

module.exports.signUp = async (req, res) => {
  const {firstName, lastName, email, password} = req.body

  try {
    const user = await UserModel.create({firstName, lastName, email, password });
    res.status(201).json({ user: user._id});
  }
  catch(err) {
    const errors = signUpErrors(err);
    res.status(200).send({ errors })
  }
}

module.exports.signIn = async (req, res) => {
  const { email, password } = req.body

  try {
    const user = await UserModel.login(email, password);
    const token = createToken(user._id);
    res.cookie('jwt', token, { httpOnly: true, maxAge});
    return res.status(200).json({ user: user._id})
  } catch (err){
    const errors = signInErrors(err);
    res.status(200).json({ errors });
  }
},
module.exports.checkUser = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]; 
    if (token) {
      jwt.verify(token, process.env.RANDOM_TOKEN_SECRET, async (err, decodedToken) => {
        if (err) {
          res.locals.user = null;
          // res.cookie("jwt", "", { maxAge: 1 });
          next();
        } else {
          let user = await UserModel.findById(decodedToken.id);
          res.locals.user = user;
          next();
        }
      });
    } else {
      res.locals.user = null;
      next();
    }
  };
  
  module.exports.requireAuth = (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1]; 
    if (token) {
      jwt.verify(token, process.env.RANDOM_TOKEN_SECRET, async (err, decodedToken) => {
        if (err) {
          console.log(err);
          res.send(200).json('no token')
        } else {
          console.log(decodedToken.id);
          next();
        }
      });
    } else {
      console.log('No token');
    }
  };
  